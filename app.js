
// const http = require('http');

// const hostname = '127.0.0.1';
// const port = 10000;

// const server = http.createServer((req, res) => {
//     res.statusCode = 200;

//     res.setHeader('Content-type', 'text/plan');
//     res.end("Hello World PCR");
// } );

// server.listen(port, hostname, () => {
//     console.log(`App Running on port ${port}`);
// });

// nodejs with express

const express       = require('express')
const cors          = require('cors')
const helmet        = require('helmet')
const bodyParser    = require('body-parser')
const mongoose      = require('mongoose')
const app           = express();

const mongoUri = 'mongodb+srv://admin:admin@cluster0-vy9ez.mongodb.net/test?retryWrites=true&w=majority';
const conectDB = () => 
    mongoose.connect(mongoUri, {
        useNewUrlParser: true,
        useCreateIndex: true,
    })
    .then(() => console.log('DB Connected!'))
    .catch(() => console.log('Failed to Connect DB!'));

    conectDB();

app.use(bodyParser.json())
app.use(cors())
app.use(helmet())

const {
    userList,
    addUser,
    getUserById,
    editUser,
    deleteUser,
} = require('./modules/users')

const {
    login,
} = require('./modules/auth');

app.get('/users', userList);
app.post('/login', login);
app.post('/adduser', addUser);
app.put('/user/:id', editUser);
app.delete('/user/:id', deleteUser);
app.get('/user/:id', getUserById);

// app.get('/', (req, res, next) => res.json({status : 'succes', code : '200'} ))
// app.get('/index', (req, res) => res.send('succes' ))

app.listen(8888, () => {
    console.log(`App Running Up!`)
})
